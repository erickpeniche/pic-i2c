#include "Config.h"

void config_board(){
	/*--------76543210*/
	TRISA = 0b10000000;															// RA7/CLKIN => INPUT (CRYSTAL OSC)
	TRISB = 0b11000000;															// RB7/ICSPDAT => INPUT (PICKIT), RB6/ICSPCLK => INPUT (PICKIT)
	TRISC = 0b00000000;															//
	TRISD = 0b00000000;															//
	TRISE = 0b00000000;

	PORTA = PORTB = PORTC = PORTD = PORTE = 0;									// All ports are cleared

	/* ANALOG ports */
	ANSEL  = 0;														// ANS<7:0> disabled
	ANSELH = 0;														// ANS disabled

	INTCONbits.GIE = 1;															// Enables Global Interrupts
	INTCONbits.PEIE = 1;														// Enables Peripherial Interrupts
}