#ifndef DS1307_H
#define	DS1307_H

#include <pic16f887.h>
#include "GlobalDefinitions.h"
#include "I2C.h"

#define I2C_SLAVE 104
#define I2C_WRITE 0
#define I2C_READ 1

void ds1307_write(unsigned char address, unsigned char command);
unsigned char ds1307_read(unsigned char address);

unsigned char ds1307GetSec();
unsigned char ds1307GetMin();
unsigned char ds1307GetHours();
unsigned char ds1307GetDay();

void ds1307SetSecond(unsigned char newSec, unsigned char chBit);
void ds1307SetMinutes(unsigned char newMin);
void ds1307SetHours12(unsigned char newHour, unsigned char pm_nAm);
void ds1307SetHour24(unsigned char newHour);
void ds1307SetDay(unsigned char newDay);
void ds1307separateDigits(unsigned char num2);

unsigned char digitPlaceVal[2];

void ds1307_write(unsigned char address, unsigned char command){
	i2c_Start();                        // send Start
	i2c_Address(I2C_SLAVE, I2C_WRITE);  // Send slave address - write operation
	i2c_Write(address);	                // DS1307
	i2c_Write(command);	                // 2 seconds on servo 0
	i2c_Stop();	                        // send Stop
}

unsigned char ds1307_read(unsigned char address){
	unsigned char read_byte;
	// Read one byte (i.e. servo pos of one servo)
	i2c_Start();                        // send Start
	i2c_Address(I2C_SLAVE, I2C_WRITE);  // Send slave address - write operation
	i2c_Write(address);	                // Set register for servo 0
	i2c_Restart();                      // Restart
	i2c_Address(I2C_SLAVE, I2C_READ);   // Send slave address - read operation
	read_byte = i2c_Read(0);            // Read one byte
						  // If more than one byte to be read, (0) should
						  // be on last byte only
						  // e.g.3 bytes= i2c_Read(1); i2c_Read(1); i2c_Read(0);
	i2c_Stop();                         // send Stop
	return read_byte;                   // return byte.
										// If reading more than one byte
										// store in an array
}

unsigned char ds1307GetSec(){
	unsigned char sec10;
	unsigned char sec1;

	unsigned char bcdSec = ds1307_read(0);//get the 0th location data(CH + 10Sec + 1Sec)[in BCD format]

	bcdSec = bcdSec & 0b1111111;//ignore CH bit (ignore MSB)
	sec10 = bcdSec >> 4;//shift 4 bits right to ignore 1sec position(ignore 4 LSB)
	sec1 = bcdSec & 0b1111;//ignore 4 MSBs

	return sec10 * 10 + sec1;//return the seconds
}

unsigned char ds1307GetMin(){
	unsigned char min10;
	unsigned char min1;

	unsigned char bcdMin = ds1307_read(1);//get the 1st loaction's data(0 + 10Min + 1Min)[in BCD format]

	bcdMin = bcdMin & 0b1111111;//ignore MSB
	min10 = bcdMin >> 4;//shift 4 bit right to ignore 1min position(ignore 4 LSB)
	min1 = bcdMin &0b1111;

	return min10 * 10 + min1;//return the minnutes
}

unsigned char ds1307GetHours(){
	unsigned char hour10;
	unsigned char hour1;
	unsigned char retHour;

	unsigned char bcdHour = ds1307_read(2);//get the 1st loaction's data(0 + 12 or 24 hour mode + 10hour(first bit) or PM/AM + 10hour(second bit) + 1hour)[in BCD format]
	bcdHour = bcdHour & 0b1111111;//ignore MSB(7th bit)
	if(bcdHour > 63){//is in 12 hours mode?
		bcdHour = bcdHour & 0b111111;//ignore MSB(6th bit)

		hour10 = (bcdHour & 0b11111) >> 4;//get the hour10 position by ignoring MSB(5th bit) and shift 4 bits to right
		hour1 = bcdHour & 0b1111;//get hour1 position by getting only 4 LSBs
		retHour = hour10 * 10 + hour1;//calculate the hours using hour10 and hour1

		if(bcdHour > 31){//is PM?
			if(retHour != 12)
				retHour = retHour + 12;
		}
	}else{
		bcdHour = bcdHour & 0b111111;//ignore MSB(6th bit)
		hour10 = bcdHour >> 4;//shift 4 bit to right to get 5th and 4th bits
		hour1 = bcdHour & 0b1111;//get 4 LSBs

		retHour = hour10 * 10 + hour1;//calculate the hours using hour10 and hour1
	}

	return retHour;
}

unsigned char ds1307GetDay(){
	return ds1307_read(3);				//read value on 3rd location of DS1307 and return it
}

/**
 * set seconds to DS1307 with CH bit
 * @param newSec - seconds to be set (0 - 79)
 * @param chBit - Clock enable bit (0 = clock enable, 1 = clock disable)
 */
void ds1307SetSecond(unsigned char newSec, unsigned char chBit){
	unsigned char bcdNum;
	if(newSec > 79)
		return;//to avoid writing to CH when writing to second feild

	ds1307separateDigits(newSec);
	bcdNum = digitPlaceVal[1] << 4;//shift 4 bits left
	bcdNum = bcdNum | digitPlaceVal[0];//ORing with placeValue1

	//add CH bit
	if(chBit == 1)
		bcdNum = bcdNum | 0b10000000;

	ds1307_write(0, bcdNum);//write to sec
}

/**
 * Set DS1307 minutes
 * @param newMin - minutes to be set(0 - 127)
 */
void ds1307SetMinutes(unsigned char newMin){
	unsigned char bcdNum;
	if(newMin > 127)
		return;//to avoid overlimit

	ds1307separateDigits(newMin);
	bcdNum = digitPlaceVal[1] << 4;//shift 4 bits left
	bcdNum = bcdNum | digitPlaceVal[0];//ORing with placeValue1

	ds1307_write(1, bcdNum);//write to min
}

/**
 * Set DS1307 hours in 12 hour mode
 * @param newHour - hours in 12 hour mode (0 - 19)
 * @param pm_nAm - AM/PM bit (1 for PM, 0 for AM)
 */
void ds1307SetHours12(unsigned char newHour, unsigned char pm_nAm){
	unsigned char bcdNum;
	if(newHour > 19)
		return;//avoid overlimit

	ds1307separateDigits(newHour);
	bcdNum = digitPlaceVal[1] << 4;//place hour's placeValue10
	bcdNum = bcdNum | digitPlaceVal[0];//ORing with placeValue1

	bcdNum = bcdNum | 0b1000000;//set 12 hour mode
	if(pm_nAm)//PM?
		bcdNum = bcdNum | 0b100000;//set PM

	ds1307_write(2, bcdNum);//write to hours
}

/**
 * Set DS1307 hours in 24 hour mode
 * @param newHour - hours in 24 hour mode (0 - 29)
 */
void ds1307SetHour24(unsigned char newHour){
	unsigned char bcdNum;
	if(newHour > 29)
		return;//avoid overlimit

	ds1307separateDigits(newHour);
	bcdNum = digitPlaceVal[1] << 4;//place hour's placeValue10
	bcdNum = bcdNum | digitPlaceVal[0];//ORing with placeValue1

	ds1307_write(2, bcdNum);//write to hours
}

/**
 * Set day to DS1307 (range 0-7)
 * @param newDay - day to be set (1 = SUN, 2 = MON,...., 7 = SAT)
 */
void ds1307SetDay(unsigned char newDay){
	if(newDay > 7)
		return;//avoid overlimit

	ds1307_write(3, newDay);//write to day
}

/**
 * This function accept 2 digit number and separate it into 10 and 1 place value and assign to digit array
 *
digit[0] = place value 1
 *
digit[1] = place value 10
 * @param num2 - 2 digit number(0 - 99)
 */
void ds1307separateDigits(unsigned char num2){
	digitPlaceVal[1] = num2 / 10;
	digitPlaceVal[0] = num2 - (digitPlaceVal[1] * 10);
}

#endif	/* DS1307_H */

