#ifndef I2C_H
#define	I2C_H

#include <pic16f887.h>
#include "GlobalDefinitions.h"

#define I2C_WRITE 0
#define I2C_READ 1

#define SCL_IO TRISCbits.TRISC3
#define SDA_IO TRISCbits.TRISC4
#define SCL_PORT PORTCbits.RC3
#define SDA_PORT PORTCbits.RC4

#define SEN SSPCON2bits.SEN
#define RSEN SSPCON2bits.RSEN
#define PEN SSPCON2bits.PEN
#define RCEN SSPCON2bits.RCEN
#define ACKDT SSPCON2bits.ACKDT
#define ACKEN SSPCON2bits.ACKEN

void i2c_Init(void);
void i2c_Wait(void);
void i2c_Start(void);
void i2c_Restart(void);
void i2c_Stop(void);
void i2c_Write(unsigned char data);
void i2c_Address(unsigned char address, unsigned char mode);
unsigned char i2c_Read(unsigned char ack);

void i2c_Init(void){
	// Initialise I2C MSSP
	// Master 100KHz
	SCL_IO = 1;           	// set SCL and SDA pins as inputs
	SDA_IO = 1;

	/*--Bit----76543210---*/
	SSPCON = 0b00101000; 	// I2C enabled, Master mode
	SSPCON2 = 0;

	// I2C Master mode, clock = FOSC/(4 * (SSPADD + 1))
	SSPADD = 9;    		// 100Khz @ 4Mhz Fosc

	/*---Bit----76543210---*/
	SSPSTAT = 0b10000000; 	// Slew rate disabled
}

// i2c_Wait - wait for I2C transfer to finish
void i2c_Wait(void){
	while ((SSPCON2 & 0b00011111) || (SSPSTAT & 0b00000100));
}

// i2c_Start - Start I2C communication
void i2c_Start(void){
	i2c_Wait();
	SEN=1;
}

// i2c_Restart - Re-Start I2C communication
void i2c_Restart(void){
	i2c_Wait();
	RSEN=1;
}

// i2c_Stop - Stop I2C communication
void i2c_Stop(void){
	i2c_Wait();
	PEN=1;
}

// i2c_Write - Sends one byte of data
void i2c_Write(unsigned char data){
	i2c_Wait();
	SSPBUF = data;
}

// i2c_Address - Sends Slave Address and Read/Write mode
// mode is either I2C_WRITE or I2C_READ
void i2c_Address(unsigned char address, unsigned char mode){
	unsigned char l_address;

	l_address=address<<1;
	l_address+=mode;
	i2c_Wait();
	SSPBUF = l_address;
}

// i2c_Read - Reads a byte from Slave device
unsigned char i2c_Read(unsigned char ack){
	// Read data from slave
	// ack should be 1 if there is going to be more data read
	// ack should be 0 if this is the last byte of data read
	unsigned char i2cReadData;

	i2c_Wait();
	RCEN=1;
	i2c_Wait();
	i2cReadData = SSPBUF;
	i2c_Wait();
	if (ack) ACKDT=0;			// Ack
	else       ACKDT=1;			// NAck
	ACKEN=1;   		            // send acknowledge sequence

	return( i2cReadData );
}

#endif	/* I2C_H */