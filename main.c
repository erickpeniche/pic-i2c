#include <stdio.h>
#include <stdlib.h>
#include <xc.h>
#include "Config.h"
#include "lcd.h"
#include "I2C.h"
#include "DS1307.h"

/**
* PIC Configurations
*/
#pragma config FOSC = XT
#pragma config WDTE = OFF
#pragma config PWRTE = OFF
#pragma config MCLRE = ON
#pragma config CP = OFF
#pragma config CPD = OFF
#pragma config BOREN = OFF
#pragma config IESO = OFF
#pragma config FCMEN = OFF
#pragma config LVP = OFF

#pragma config BOR4V = BOR40V
#pragma config WRT = OFF

void start_lcd(void);
void display_time(void);

unsigned char buffer1[20];
unsigned char buffer2[20];

unsigned char hours = 0;
unsigned char minutes = 0;
unsigned char seconds = 0;

int main(int argc, char** argv) {
	config_board();
	start_lcd();
	i2c_Init();

	ds1307SetSecond(0, 0);
	ds1307SetMinutes(9);
	ds1307SetHours12(2, 0);

	while(1){
		display_time();
		//__delay_ms(900);
	}

	return (EXIT_SUCCESS);
}

void start_lcd(void) {
	Lcd_Init();
	Lcd_Cmd(LCD_CLEAR);
	Lcd_Cmd(LCD_CURSOR_OFF);
	__delay_ms(100);
}

void display_time(void) {
	seconds = ds1307GetSec();
	minutes = ds1307GetMin();
	hours = ds1307GetHours();

	sprintf(buffer1, "%02d:%02d:%02d", hours, minutes, seconds);									//convertimos el valor en ASCII
	Lcd_Out2(1, 1, buffer1);
}