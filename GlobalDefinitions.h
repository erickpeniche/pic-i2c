#ifndef GLOBALDEFINITIONS_H
#define	GLOBALDEFINITIONS_H

#define true  1
#define false 0

#define _ON  1
#define _OFF 0

#define _INPUT 1
#define _OUTPUT 0

#define SCLPORT TRISCbits.TRISC3
#define SDAPORT TRISCbits.TRISC4

#endif	/* GLOBALDEFINITIONS_H */